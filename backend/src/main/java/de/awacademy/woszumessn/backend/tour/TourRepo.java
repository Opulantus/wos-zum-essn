package de.awacademy.woszumessn.backend.tour;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TourRepo extends CrudRepository<Tour, Integer> {

    List<Tour> findAll();

    Optional<Tour> findById(Integer id);
}
