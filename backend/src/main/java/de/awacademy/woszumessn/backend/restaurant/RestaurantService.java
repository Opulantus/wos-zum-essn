package de.awacademy.woszumessn.backend.restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RestaurantService {

    private RestaurantRepo restaurantRepo;

    @Autowired
    public RestaurantService(RestaurantRepo restaurantRepo) {
        this.restaurantRepo = restaurantRepo;
    }

    public List<Restaurant> getAllRestaurants() {
        return restaurantRepo.findAll();
    }
    public Restaurant getRestaurant(Integer id) {return restaurantRepo.findById(id).get();}

   /* public void addRestaurant(Restaurant restaurant) {
        restaurantRepo.save(restaurant);
    }*/
}
