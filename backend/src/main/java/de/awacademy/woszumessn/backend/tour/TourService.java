package de.awacademy.woszumessn.backend.tour;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TourService {

    private TourRepo tourRepo;

    @Autowired
    public TourService(TourRepo tourRepo) {
        this.tourRepo = tourRepo;
    }

    public List<Tour> getAllTours() {
        return tourRepo.findAll();
    }
    public Tour getSpecificTour(Integer id) {return tourRepo.findById(id).get();}

}
