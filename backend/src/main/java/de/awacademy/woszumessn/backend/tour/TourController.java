package de.awacademy.woszumessn.backend.tour;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TourController {

    private TourService tourService;

    @Autowired
    public TourController(TourService tourService) {
        this.tourService = tourService;
    }

    @GetMapping("/api/tours")
    public List<Tour> showAllTours() {
        List<Tour> tourList = tourService.getAllTours();
        return tourList;
    }

/*    @GetMapping("/api/tour/{id}")
    public Tour showTour(@PathVariable("id") Integer id) {
        Tour tour = tourService.getSpecificTour(id);
        return tour;
    }*/
}
