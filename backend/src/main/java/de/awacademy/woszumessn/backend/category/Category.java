package de.awacademy.woszumessn.backend.category;

import de.awacademy.woszumessn.backend.restaurant.Restaurant;

import javax.persistence.*;
import java.util.List;

@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String categoryName;

    @OneToMany
    private List<Restaurant> restaurants;

    public Category() {
    }

    public Integer getCategoryId() {
        return id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }
}
