package de.awacademy.woszumessn.backend.guide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuideService {

    private GuideRepo guideRepo;

    @Autowired
    public GuideService(GuideRepo guideRepo) {
        this.guideRepo = guideRepo;
    }

    public List<Guide> getAllGuides() {
        return guideRepo.findAll();
    }

    public Guide getSpecificGuide(Integer id) {
        return guideRepo.findById(id).get();
    }

}
