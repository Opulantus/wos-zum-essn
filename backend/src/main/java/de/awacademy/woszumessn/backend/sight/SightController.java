package de.awacademy.woszumessn.backend.sight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SightController {

    private SightService sightService;

    @Autowired
    public SightController(SightService sightService) {
        this.sightService = sightService;
    }

    @GetMapping("/api/sights")
    public List<Sight> showAllSights() {
        List<Sight> sightList = sightService.getAllSights();
        return sightList;
    }

    @GetMapping("/api/sight/{id}")
    public Sight showTour(@PathVariable("id") Integer id) {
        Sight sight = sightService.getSpecificSight(id);
        return sight;
    }
}
