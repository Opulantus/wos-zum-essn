package de.awacademy.woszumessn.backend.guide;

import de.awacademy.woszumessn.backend.restaurant.Restaurant;
import de.awacademy.woszumessn.backend.tour.Tour;

import javax.persistence.*;

@Entity
public class Guide {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String GuideName;
    private String description;
    private String motto;
    private String pictureUrl;

    /*@OneToOne
    private Tour tour;*/

    public Guide() {
    }

    public Integer getId() {
        return id;
    }

    public String getGuideName() {
        return GuideName;
    }

    public String getDescription() {
        return description;
    }

    public String getMotto() {
        return motto;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    /*public Tour getTour() {
        return tour;
    }*/


}
