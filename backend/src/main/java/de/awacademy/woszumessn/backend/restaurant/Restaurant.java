package de.awacademy.woszumessn.backend.restaurant;

import de.awacademy.woszumessn.backend.category.Category;

import javax.persistence.*;
import java.awt.*;
import java.util.List;

@Entity
public class Restaurant {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    private String restaurantName;
    private String description;
    private String openingHours;
    private String address;
    private String website;
    private String pictureUrl;

    @ManyToOne
    private Category category;

    private String descriptionLong;
    private String map;
    private String menu;
    private String picture1Url;
    private String picture2Url;
    private String picture3Url;
    private String picture4Url;
    private String picture5Url;

    private double lat;
    private double lng;

    // private List<Diet> dietList; evtl. noch einbinden

    public Restaurant() {
    }

    public Integer getId() {
        return id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getDescription() {
        return description;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public String getAddress() {
        return address;
    }

    public String getWebsite() {
        return website;
    }


    public String getPictureUrl() {
        return pictureUrl;
    }

    public Category getCategory() {
        return category;
    }

    public String getDescriptionLong() {
        return descriptionLong;
    }

    public String getMap() {
        return map;
    }

    public String getMenu() {
        return menu;
    }

    public String getPicture1Url() {
        return picture1Url;
    }

    public String getPicture2Url() {
        return picture2Url;
    }

    public String getPicture3Url() {
        return picture3Url;
    }

    public String getPicture4Url() {
        return picture4Url;
    }

    public String getPicture5Url() {
        return picture5Url;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
