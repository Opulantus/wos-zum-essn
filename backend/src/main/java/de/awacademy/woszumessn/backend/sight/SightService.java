package de.awacademy.woszumessn.backend.sight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SightService {

    private SightRepo sightRepo;

    @Autowired
    public SightService(SightRepo sightRepo) {
        this.sightRepo = sightRepo;
    }

    public List<Sight> getAllSights() {
        return sightRepo.findAll();
    }

    public Sight getSpecificSight(Integer id) {
        return sightRepo.findById(id).get();
    }

}
