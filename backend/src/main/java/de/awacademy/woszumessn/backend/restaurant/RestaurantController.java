package de.awacademy.woszumessn.backend.restaurant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RestaurantController {

    private RestaurantService restaurantService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping("/api/restaurants")
    public List<Restaurant> showAllRestaurants() {
        List<Restaurant> restaurantList = restaurantService.getAllRestaurants();
        return restaurantList;
    }


    @GetMapping("/api/restaurant/{id}")
    public Restaurant showRestaurant(@PathVariable("id") Integer id) {
        Restaurant restaurant = restaurantService.getRestaurant(id);
        return restaurant;
    }
    /*@PostConstruct
    public void test() {
        restaurantService.addRestaurant(new Restaurant("blubb"));
        restaurantService.addRestaurant(new Restaurant("Assi"));
    }*/
}
