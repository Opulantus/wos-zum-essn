package de.awacademy.woszumessn.backend.tour;

import de.awacademy.woszumessn.backend.category.Category;
import de.awacademy.woszumessn.backend.guide.Guide;

import javax.persistence.*;

@Entity
public class Tour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String tourName;
    private String description;
    private String duration;
    private String startingPoint;
    private int numberOfParticipants;
    private String pictureUrl;
    private float price;

    /*@OneToOne
    private Guide guide;*/

    // private List<Restaurant>

    public Tour() {
    }

    public Integer getId() {
        return id;
    }

    public String getTourName() {
        return tourName;
    }

    public String getDescription() {
        return description;
    }

    public String getDuration() {
        return duration;
    }

    public String getStartingPoint() {
        return startingPoint;
    }

    public int getNumberOfParticipants() {
        return numberOfParticipants;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public float getPrice() {
        return price;
    }

    /*public Guide getGuide() {
        return guide;
    }*/
}
