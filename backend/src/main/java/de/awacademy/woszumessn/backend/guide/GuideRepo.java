package de.awacademy.woszumessn.backend.guide;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GuideRepo extends CrudRepository<Guide, Integer> {

    List<Guide> findAll();

    Optional<Guide> findById(Integer id);
}
