package de.awacademy.woszumessn.backend.restaurant;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantRepo extends CrudRepository<Restaurant, Integer> {

    List<Restaurant> findAll();

    Optional<Restaurant> findById(Integer id);
}
