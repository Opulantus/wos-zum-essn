package de.awacademy.woszumessn.backend.sight;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Sight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String sightName;
    private String description;
    private String openingHours;
    private String address;
    private String entranceFee;
    private String pictureUrl;

    public Sight() {
    }

    public Integer getId() {
        return id;
    }

    public String getSightName() {
        return sightName;
    }

    public String getDescription() {
        return description;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public String getAddress() {
        return address;
    }

    public String getEntranceFee() {
        return entranceFee;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }
}
