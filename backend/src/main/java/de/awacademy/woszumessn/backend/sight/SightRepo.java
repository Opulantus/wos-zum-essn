package de.awacademy.woszumessn.backend.sight;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SightRepo extends CrudRepository<Sight, Integer> {

    List<Sight> findAll();

    Optional<Sight> findById(Integer id);
}
