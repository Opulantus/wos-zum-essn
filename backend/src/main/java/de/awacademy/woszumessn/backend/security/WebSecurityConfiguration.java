package de.awacademy.woszumessn.backend.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic();
        // http.authorizeRequests().anyRequest().authenticated();
        http.authorizeRequests().antMatchers("/api/restaurants").permitAll();
        http.authorizeRequests().antMatchers("/api/categories").permitAll();
        http.authorizeRequests().antMatchers("/api/restaurant/{id}").permitAll();
        http.authorizeRequests().antMatchers("/api/tours").permitAll();
        http.authorizeRequests().antMatchers("/api/tour/{id}").permitAll();
        http.authorizeRequests().antMatchers("/api/sights").permitAll();
        http.authorizeRequests().antMatchers("/api/sight/{id}").permitAll();
        http.authorizeRequests().antMatchers("/api/guides").permitAll();
        http.authorizeRequests().antMatchers("/api/guide/{id}").permitAll();
        // Neu
        http.exceptionHandling()
            .authenticationEntryPoint(new Http403ForbiddenEntryPoint());

        http.logout().logoutUrl("/api/logout")
            .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());

        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());

    }
}
