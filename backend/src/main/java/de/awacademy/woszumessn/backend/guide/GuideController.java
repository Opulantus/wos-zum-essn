package de.awacademy.woszumessn.backend.guide;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GuideController {

    private GuideService guideService;

    @Autowired
    public GuideController(GuideService guideService) {
        this.guideService = guideService;
    }

    @GetMapping("/api/guides")
    public List<Guide> showAllGuides() {
        List<Guide> guideList = guideService.getAllGuides();
        return guideList;
    }

    @GetMapping("/api/guide/{id}")
    public Guide showGuide(@PathVariable("id") Integer id) {
        Guide guide = guideService.getSpecificGuide(id);
        return guide;
    }
}
