import {Guide} from './guide';

export interface Tour {
  id: number;
  tourName: string;
  description: string;
  duration: string;
  startingPoint: string;
  numberOfParticipants: string;
  pictureUrl: string;
  price: number;
  // guide: Guide;
}
