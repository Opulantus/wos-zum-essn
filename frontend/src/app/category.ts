import {Restaurant} from './restaurant';

export interface Category {
  id: number;
  categoryName: string;
  restaurant: Restaurant;
}
