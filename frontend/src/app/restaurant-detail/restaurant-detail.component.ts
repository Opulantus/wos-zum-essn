import {Component, Input, OnInit} from '@angular/core';
import {Restaurant} from '../restaurant';
import {ActivatedRoute} from '@angular/router';
import {RestaurantService} from '../restaurant.service';
import {HttpClient} from '@angular/common/http';
import {CategoryListComponent} from '../category-list/category-list.component';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: ['./restaurant-detail.component.css']
})
export class RestaurantDetailComponent implements OnInit {

  selectedRestaurant: Restaurant;

  constructor(private catList: CategoryListComponent) {
  }

  ngOnInit() {
    this.selectedRestaurant = this.catList.selectedRestaurant;
  }

}
