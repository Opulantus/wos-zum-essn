import {Component, Input, OnInit} from '@angular/core';
import {Restaurant} from '../restaurant';
import {HttpClient} from '@angular/common/http';
import {Category} from '../category';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {

  @Input() category: Category;
  restaurants: Restaurant[];
  selectedRestaurant: Restaurant;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<Restaurant[]>('api/restaurants').subscribe(restaurants => this.restaurants = restaurants);
  }

  onSelectRest(r: Restaurant): void {
    this.selectedRestaurant = r;
  }

}
