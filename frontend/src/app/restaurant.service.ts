import { Injectable } from '@angular/core';
import {Restaurant} from './restaurant';
import {HttpClient} from '@angular/common/http';
import {applySourceSpanToExpressionIfNeeded} from '@angular/compiler/src/output/output_ast';


@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  private restaurants: Restaurant[];

  constructor(private http: HttpClient) {
  }

  getRestaurant(id: number): Restaurant {
    console.log('Funktioniert vielleicht...');
    return this.restaurants.find(r => r.id === id);
  }
}
