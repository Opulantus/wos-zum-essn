import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CategoryListComponent} from './category-list/category-list.component';
import {RestaurantDetailComponent} from './restaurant-detail/restaurant-detail.component';
import {IndexComponent} from './index/index.component';
import {TourListComponent} from './tour-list/tour-list.component';
import {GuideListComponent} from './guide-list/guide-list.component';


const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'restaurants', component: CategoryListComponent},
  {path: 'tours', component: TourListComponent},
  {path: 'guides', component: GuideListComponent},
  {path: 'restaurant/:id', component: RestaurantDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
