import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SessionUserComponent } from './session-user/session-user.component';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { RestaurantListComponent } from './restaurant-list/restaurant-list.component';
import { CategoryListComponent} from './category-list/category-list.component';
import { RestaurantDetailComponent } from './restaurant-detail/restaurant-detail.component';
import { IndexComponent } from './index/index.component';
import { TourListComponent } from './tour-list/tour-list.component';
import { AgmCoreModule } from '@agm/core';
import {GuideListComponent} from './guide-list/guide-list.component';
import {SightComponent} from './sight/sight.component';

@NgModule({
  declarations: [
    AppComponent,
    SessionUserComponent,
    LoginComponent,
    RestaurantListComponent,
    CategoryListComponent,
    RestaurantDetailComponent,
    IndexComponent,
    TourListComponent,
    GuideListComponent,
    SightComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBk5el2ZnvBYAi2hv_FpSQkDBVboqeldZc'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
