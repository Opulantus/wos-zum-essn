import {Component, OnInit} from '@angular/core';
import {Tour} from '../tour';
import {HttpClient} from '@angular/common/http';
import {Category} from '../category';
import {Restaurant} from '../restaurant';

@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.component.html',
  styleUrls: ['./tour-list.component.css']
})
export class TourListComponent implements OnInit {

  tours: Tour[];
  selectedTour: Tour;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<Tour[]>('api/tours').subscribe(tours => this.tours = tours);
  }

  onSelect(tour: Tour): void {
    this.selectedTour = tour;
  }

  onSelectTour(t: Tour): void {
    this.selectedTour = t;
  }

}
