import {Category} from './category';

export interface Restaurant {
  id: number;
  restaurantName: string;
  description: string;
  openingHours: string;
  address: string;
  website: string;
  pictureUrl: string;
  category: Category;
  descriptionLong: string;
  map: string;
  menu: string ;
  picture1Url: string;
  picture2Url: string;
  picture3Url: string;
  picture4Url: string;
  picture5Url: string;
  lat: number;
  lng: number;
}
