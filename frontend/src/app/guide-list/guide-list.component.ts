import {Component, OnInit} from '@angular/core';
import {Guide} from '../guide';
import {HttpClient} from '@angular/common/http';
import {Tour} from '../tour';

@Component({
  selector: 'app-guide',
  templateUrl: './guide-list.component.html',
  styleUrls: ['./guide-list.component.css']
})
export class GuideListComponent implements OnInit {

  guides: Guide[];
  selectedGuide: Guide;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<Guide[]>('api/guides').subscribe(guides => this.guides = guides);
  }

  onSelect(guide: Guide): void {
    this.selectedGuide = guide;
  }
}
