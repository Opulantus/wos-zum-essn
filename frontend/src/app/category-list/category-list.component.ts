import {Component, OnInit} from '@angular/core';
import {Category} from '../category';
import {HttpClient} from '@angular/common/http';
import {Restaurant} from '../restaurant';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  categories: Category[];
  selectedCategory: Category;
  restaurants: Restaurant[];
  selectedRestaurant: Restaurant;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<Category[]>('api/categories').subscribe(categories => this.categories = categories);
    this.http.get<Restaurant[]>('api/restaurants').subscribe(restaurants => this.restaurants = restaurants);
  }

  onSelect(category: Category): void {
    this.selectedCategory = category;
  }

  onSelectRest(r: Restaurant): void {
    this.selectedRestaurant = r;
  }

}
