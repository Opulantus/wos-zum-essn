export interface Sight {
  id: number;
  sightName: string;
  description: string;
  openingHours: string;
  address: string;
  entranceFee: string;
  pictureUrl: string;
}
