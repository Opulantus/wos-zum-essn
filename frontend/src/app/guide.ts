import {Restaurant} from './restaurant';
import {Tour} from './tour';

export interface Guide {
  id: number;
  guideName: string;
  description: string;
  motto: string;
  pictureUrl: string;
  // tour: Tour;
}
