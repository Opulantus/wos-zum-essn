ReadMe for WOS ZUM ESSN

The app Wos zum Essn is a food guide for Garching (and Munich in the future):
You can use it to find restaurants and food-tours including tour-guides.

It's a single-page-application developed using the following technologies:

In the backend:
- Java 11
- Spring (Boot & Security)
- HeidiSQL

This frontend was generated with Angular CLI version 8.2.1.:
- TypeScript
- HTML 5
- CSS
- JavaScript